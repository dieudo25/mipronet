from rest_framework import routers

from .views import FlexPageSet


# Rest Framework API
flex_page_router = routers.DefaultRouter()

flex_page_router.register(r"pages", FlexPageSet)