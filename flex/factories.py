from factory import (
    DjangoModelFactory,
    LazyAttribute,
    Sequence,
)
from factory.fuzzy import FuzzyText
from django.utils.text import slugify

from wagtail_factories import PageFactory

from flex.models import FlexPage


class FlexPageFactory(PageFactory):
    """
        FlexPageFactory used to create model for UnitTest
    """

    class Meta:
        model = FlexPage

    # Would make the page has title like FlexPage 1, FlexPage 2, and so on.
    title = Sequence(lambda n: "FlexPage %d" % n)
    description = Sequence(lambda n: "Description %d" % n)