from rest_framework import serializers
from wagtailstreamforms.models import Form

from wagtail.core.models import Page


class LinkPageSerialzer(serializers.ModelSerializer):

    class Meta:
        model = Page
        fields = [
            "id",
            "title",
            "slug",
            "content_type",
        ]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.content_type:
            representation['content_type'] = f"{instance.content_type.app_label}.{instance.content_type.model}"
        return representation


class WagtailFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = [
            "id",
            "title",
            "slug",
            "fields",
            "submit_button_text",
            "success_message",
            "error_message",
            "post_redirect_page"

        ]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.fields:
            representation['fields'] = [{
                "type": item.block_type,
                "value": item.value
            } for item in instance.fields]
        if instance.post_redirect_page:
            representation['post_redirect_page'] = {
                "id": f"{instance.id}",
                "title": f"{instance.post_redirect_page.title}",
                "slug": f"{instance.post_redirect_page.slug}",
                "content_type": f"{instance.post_redirect_page.content_type.app_label}.{instance.post_redirect_page.content_type.model}",
            }
        return representation
