from wagtail.core import blocks
from wagtail.images.api.fields import ImageRenditionField
from wagtail.images.blocks import ImageChooserBlock

from wagtailstreamforms.blocks import FormChooserBlock, WagtailFormBlock

from .serializers_cms import LinkPageSerialzer, WagtailFormSerializer


class OffSetBlock(blocks.StructBlock):
    block_id = blocks.CharBlock(required=False)
    class_style = blocks.CharBlock(required=False)


class ReactReavealBlock(blocks.StructBlock):
    reveal = blocks.ChoiceBlock(choices=[
        ('left', 'Gauche'),
        ('right', 'Droite'),
        ('top', 'Haut'),
        ('bottom', 'Bas'),
        ('clear', 'Centre')
    ], required=False, blank=True, null=True)
    reveal_big = blocks.BooleanBlock(required=False, default=False)
    reveal_cascade = blocks.BooleanBlock(required=False, default=False)


class HeadingsBlock(blocks.StructBlock):
    heading_type = blocks.ChoiceBlock(choices=[
        ('h1', 'H1'),
        ('h2', 'H2'),
        ('h3', 'H3'),
        ('h4', 'H4'),
        ('h5', 'H5'),
        ('h6', 'H6'),
    ], required=False, blank=True, null=True)
    text = blocks.CharBlock(required=False, blank=True, null=True)
    style_classes = blocks.CharBlock(required=False)
    container_width = blocks.ChoiceBlock(choices=[
        ('normal-width', 'Largeur normal'),
        ('full-width', 'Toute la largeur'),
    ], required=False, default='normal-width')
    animation = ReactReavealBlock(required=False)


class CustomImageChooserBlock(ImageChooserBlock):
    """
        CustomImageChooserBlocks
        Parameters:
            rendition: defaulft value original
    """

    def __init__(self, *args, **kwargs):
        """
            function will be launch when the object is created (initiated)
        """
        self.rendition = kwargs.pop("rendition", "original")
        super().__init__(**kwargs)

    def get_api_representation(self, value, context=None):
        """
            Function used to generate new size for the image
            and add a new represetation to the API field based on ImageRenditionField
        """
        return ImageRenditionField(self.rendition).to_representation(value)


class AnimatedImageBlock(blocks.StructBlock):
    """
    Image having animation
    """

    image = CustomImageChooserBlock()
    animation = ReactReavealBlock(required=False)
    classes = blocks.CharBlock(required=False)


WagtailFormBlock


class CustomFormChooserBlock(FormChooserBlock):
    def get_api_representation(self, value, context=None):
        if (value):
            return WagtailFormSerializer(context=context).to_representation(value)


class CustomWagtailFormBlock(WagtailFormBlock):
    form = CustomFormChooserBlock()
    re_captcha = blocks.BooleanBlock()
    animation = ReactReavealBlock(required=False)


class CustomPageChooserBlock(blocks.PageChooserBlock):
    def get_api_representation(self, value, context=None):
        if (value):
            return LinkPageSerialzer(context=context).to_representation(value)


class ButtonBlock(blocks.StructBlock):
    text = blocks.CharBlock(required=False, blank=True, null=True)
    page = CustomPageChooserBlock(required=False, blank=True, null=True)
    external_url = blocks.URLBlock(required=False, blank=True, null=True)
    style_type = blocks.ChoiceBlock(choices=[
        ('primary', 'Primaire'),
        ('secondary', 'Secondaire'),
    ], required=False, default='primary')
    style_classes = blocks.CharBlock(required=False, blank=True, null=True)


class CardBlock(blocks.StructBlock):
    image = CustomImageChooserBlock(
        rendition=f"width-200", required=True)
    text = blocks.RichTextBlock(required=True)
    display = blocks.ChoiceBlock(choices=[
        ('row', 'Horizontal'),
        ('column', 'Vertical'),
    ])
    style_class = blocks.CharBlock(required=False)
    page_link = CustomPageChooserBlock(required=False)
    anchor = blocks.CharBlock(required=False, blank=True, null=True)
    animation = ReactReavealBlock(required=False)


class SliderItemCTABlock(blocks.StructBlock):
    """
        SliderItemBlock
    """

    title = blocks.CharBlock(required=True)
    description = blocks.RichTextBlock()
    button_text = blocks.CharBlock(required=False)
    button = CustomPageChooserBlock(required=False)
    image = CustomImageChooserBlock(rendition="width-2560", required=True)


class SlickSliderResponsiveSettingsBlock(blocks.StructBlock):
    breakpoints = blocks.IntegerBlock(required=False)
    slideToShow = blocks.IntegerBlock(required=False, default=1)
    slideToScroll = blocks.IntegerBlock(required=False, default=1)
    vertical = blocks.BooleanBlock(default=False, required=False)
    vertical_swiping = blocks.BooleanBlock(default=False, required=False)
    autoplay = blocks.BooleanBlock(default=False, required=False)
    speed = blocks.IntegerBlock(default=500, required=False)
    arrows = blocks.BooleanBlock(default=False, required=False)
    dots = blocks.BooleanBlock(default=False, required=False)
    fade = blocks.BooleanBlock(default=False, required=False)
    infinite = blocks.BooleanBlock(default=False, required=False)
    pause_on_focus = blocks.BooleanBlock(default=False, required=False)
    pause_on_hover = blocks.BooleanBlock(default=False, required=False)
    pause_on_dots_hover = blocks.BooleanBlock(default=False, required=False)


class SlickSliderSettingsBlock(blocks.StructBlock):
    slideToShow = blocks.IntegerBlock(required=False, default=1)
    slideToScroll = blocks.IntegerBlock(required=False, default=1)
    vertical = blocks.BooleanBlock(default=False, required=False)
    vertical_swiping = blocks.BooleanBlock(default=False, required=False)
    autoplay = blocks.BooleanBlock(default=False, required=False)
    speed = blocks.IntegerBlock(default=500, required=False)
    arrows = blocks.BooleanBlock(default=False, required=False)
    dots = blocks.BooleanBlock(default=False, required=False)
    fade = blocks.BooleanBlock(default=False, required=False)
    infinite = blocks.BooleanBlock(default=False, required=False)
    pause_on_focus = blocks.BooleanBlock(default=False, required=False)
    pause_on_hover = blocks.BooleanBlock(default=False, required=False)
    pause_on_dots_hover = blocks.BooleanBlock(default=False, required=False)


class SliderItemStreamBlock(blocks.StreamBlock):
    full_screen = SliderItemCTABlock()
    cards = CardBlock()
    image = CustomImageChooserBlock()
    animated_image = AnimatedImageBlock()


class SlickSliderBlock(blocks.StructBlock):
    title = HeadingsBlock(required=False, blank=True, null=True)
    text = blocks.RichTextBlock(required=False)
    items = SliderItemStreamBlock()
    settings = SlickSliderSettingsBlock()
    responsive_settings = blocks.ListBlock(
        SlickSliderResponsiveSettingsBlock(), required=False, blank=True, null=True)
    style_classes = blocks.CharBlock(required=False, blank=True, null=True)
    button = ButtonBlock(required=False, blank=True, null=True)
    animation = ReactReavealBlock(required=False)


class CustomAppendDotsSlider(blocks.StructBlock):
    title = HeadingsBlock(required=False, blank=True, null=True)
    text = blocks.RichTextBlock(required=False)
    items = SliderItemStreamBlock()
    settings = SlickSliderSettingsBlock()
    responsive_settings = blocks.ListBlock(
        SlickSliderResponsiveSettingsBlock(), required=False, blank=True, null=True)
    style_classes = blocks.CharBlock(required=False, blank=True, null=True)
    button = ButtonBlock(required=False, blank=True, null=True)
    animation = ReactReavealBlock(required=False)


class ImageParallaxBlock(blocks.StructBlock):
    image = CustomImageChooserBlock(rendition="width-2560", required=True)
    parallax_strengh = blocks.IntegerBlock(required=True)
    parallax_blur_min = blocks.IntegerBlock(required=True, default=0)
    parallax_blur_max = blocks.IntegerBlock(required=True, default=0)


class RichTextButtonBlock(blocks.StructBlock):
    text = blocks.RichTextBlock(required=True)
    button_text = blocks.CharBlock(required=False)
    button = CustomPageChooserBlock(required=False)
    container_width = blocks.ChoiceBlock(choices=[
        ('normal-width', 'Largeur normal'),
        ('full-width', 'Toute la largeur'),
    ], required=False, default='normal-width')
    style_classes = blocks.CharBlock(required=False)
    animation = ReactReavealBlock(required=False)


class StaticHeaderBlock(blocks.StructBlock):
    image = CustomImageChooserBlock()
    image_phone = CustomImageChooserBlock()
    title = blocks.CharBlock(required=False)
    description = RichTextButtonBlock()
    classes = blocks.CharBlock(required=False)


class EmbedGoogleMapsBlock(blocks.StructBlock):
    pass


class TwoColumnStreamBlock(blocks.StreamBlock):
    cards = CardBlock()
    text = blocks.RichTextBlock()
    image = CustomImageChooserBlock()
    animated_image = AnimatedImageBlock()
    rich_text_button = RichTextButtonBlock()
    slider = SlickSliderBlock()


class ThreeColumnStreamBlock(blocks.StreamBlock):
    cards = CardBlock()


class FourColumnStreamBlock(blocks.StreamBlock):
    cards = CardBlock()
    text = blocks.RichTextBlock()
    image = CustomImageChooserBlock()
    animated_image = AnimatedImageBlock()
    rich_text_button = RichTextButtonBlock()


class ContentParallaxStreamBlock(blocks.StreamBlock):
    headings = HeadingsBlock()
    rich_text_button = RichTextButtonBlock()
    slider = SlickSliderBlock()
    button = ButtonBlock()
    two_column = TwoColumnStreamBlock(min_num=2, max_num=2)
    three_column = ThreeColumnStreamBlock(min_num=3, max_num=3)
    four_column = ThreeColumnStreamBlock(min_num=4, max_num=4)


class ParallaxBlock(blocks.StructBlock):
    background_image = ImageParallaxBlock(required=True)
    content = ContentParallaxStreamBlock(required=True)
    style_classes = blocks.CharBlock(required=False)


class TwoColumnStructBlock(blocks.StructBlock):
    column = TwoColumnStreamBlock(min_num=2, max_num=2)
    classes = blocks.CharBlock(required=False)
    container_classes = blocks.CharBlock(required=False)


class FourColumnStructBlock(blocks.StructBlock):
    column = FourColumnStreamBlock(min_num=4, max_num=4)
    classes = blocks.CharBlock(required=False)
    container_classes = blocks.CharBlock(required=False)


class BodyStreamBlock(blocks.StreamBlock):
    """
        BodyBlock
    """

    headings = HeadingsBlock()
    text = blocks.RichTextBlock()
    rich_text_button = RichTextButtonBlock()
    slider = SlickSliderBlock()
    parallax = ParallaxBlock()
    button = ButtonBlock()
    static_header = StaticHeaderBlock()
    two_column = TwoColumnStructBlock()
    three_column = ThreeColumnStreamBlock(min_num=3, max_num=3)
    four_column = FourColumnStructBlock()
    streamform = CustomWagtailFormBlock()
    maps = EmbedGoogleMapsBlock()
    offset = OffSetBlock()
