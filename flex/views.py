from rest_framework import viewsets

from .models import FlexPage
from .serializers import FlexPageSerializer


class FlexPageSet(viewsets.ModelViewSet):
    serializer_class = FlexPageSerializer
    queryset = FlexPage.objects.all()
    http_method_names = ["get"]
