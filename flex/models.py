"""Flexible page"""

import urllib.parse
from django.db import models
from django.http.response import HttpResponseRedirect

from rest_framework import serializers
from wagtail.api import APIField
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.admin.edit_handlers import (
    FieldPanel,
    StreamFieldPanel,
)
from wagtail.admin.edit_handlers import TabbedInterface, ObjectList
from wagtailyoast.edit_handlers import YoastPanel
from wagtail_headless_preview.models import HeadlessPreviewMixin

from flex.blocks import BodyStreamBlock


class BasePage(HeadlessPreviewMixin, Page):
    keywords = models.CharField(default='', blank=True, max_length=100)

    edit_handler = TabbedInterface([
        ObjectList(Page.content_panels, heading=('Content')),
        ObjectList(Page.promote_panels, heading=('Promotion')),
        ObjectList(Page.settings_panels, heading=('Settings')),
        YoastPanel(
            keywords='keywords',
            title='seo_title',
            search_description='search_description',
            slug='slug'
        ),
    ])

    class Meta:
        abstract = True

    def serve(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.get_client_root_url())


class FlexPage(BasePage):
    """Flexible page class."""

    description = models.CharField(max_length=255, blank=True,)
    body = StreamField(BodyStreamBlock(), blank=True)

    # The home page can only live under the root page ( another way of limiting creation)
    parent_page_types = [
        'flex.HomePage',
        'flex.FlexPage'
    ]

    subpage_types = [
        'flex.FlexPage'
    ]

    content_panels = Page.content_panels + [
        FieldPanel("description", classname="full"),
        StreamFieldPanel("body"),
    ]

    api_fields = (
        "slug",
        "keywords",
        "description",
        "body",
        APIField("owner"),
        APIField(
            "pub_date",
            serializer=serializers.DateTimeField(
                format="%d %B %Y", source="last_published_at"),
        ),
    )

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading=('Content')),
        ObjectList(Page.promote_panels, heading=('Promotion')),
        ObjectList(Page.settings_panels, heading=('Settings')),
        YoastPanel(
            keywords='keywords',
            title='seo_title',
            search_description='search_description',
            slug='slug'
        ),
    ])

    class Meta:
        verbose_name = "Page"
        verbose_name_plural = "Pages"

    def get_preview_url(self, token):
        return urllib.parse.urljoin(
            # return the value defined in HEADLESS_PREVIEW_CLIENT_URLS in settings.py
            self.get_client_root_url(),
            f"post/{self.pk}/"
            + "?"
            + urllib.parse.urlencode(
                {"content_type": self.get_content_type_str(), "token": token}
            ),
        )

    def serve(self, request, *args, **kwargs):
        content_type = self.get_content_type_str()

        html_path = '' if content_type == 'flex.homepage' else self.slug

        return HttpResponseRedirect(
            urllib.parse.urljoin(
                self.get_client_root_url(),
                f"/{html_path}"
            )
        )


class HomePage(FlexPage):
    """Home Page"""

    parent_page_types = [
        'wagtailcore.Page'
    ]

    subpage_types = [
        'flex.FlexPage'
    ]

    max_count = 1   # Can only have one instance of home page

    class Meta:
        verbose_name = "Page d'accueil"
        verbose_name_plural = "Pages d'accueil"

    def get_preview_url(self, token):
        return urllib.parse.urljoin(
            # return the value defined in HEADLESS_PREVIEW_CLIENT_URLS in settings.py
            self.get_client_root_url(),
            "?"
            + urllib.parse.urlencode(
                {"content_type": self.get_content_type_str(), "token": token}
            ),
        )
