import json
import factory

from django.test import TestCase
from wagtail.core.models import Site
from wagtail_factories.factories import ImageFactory

from .factories import (
    FlexPageFactory,
)

class TestView(TestCase):
    """
        Test flex.views
    """

    def setUp(self):
        self.flex_page = FlexPageFactory.create()
        self.site = Site.objects.all().first()
        self.site.root_page = self.flex_page
        self.site.save()

    def test_flex_page_view_set(self):
        """
            Test FlexPageSet view
        """

        ### Act ###
        # Fetch flex pages from Rest API
        response = self.client.get(f"/api/flex/pages/")
        response_data = response.json()

        ### Assert ###
        # Test if the flex pages fetched from response_data self.equals flex_page
        assert response_data["results"][0]["id"] == self.flex_page.pk


        ### Arrange ###
        # Create a new flex page
        flex_page_2 = FlexPageFactory.create()

        ### Act ###
        # Fetch flex pages from Rest API
        response = self.client.get(f"/api/flex/pages/")
        response_data = response.json()

        ### Assert ###
        # Test if the number of page fetched equals to 2
        assert response_data["count"] == 2


class TestFlexPageCMSAPI(TestCase):
    """
        Test Wagtail API
    """

    def setUp(self):
        self.flex_page = FlexPageFactory.create()
        self.site = Site.objects.all().first()
        self.site.root_page = self.flex_page
        self.site.save()

    def test_pages(self):
        pass

    def test_flex_pages(self):
        """Test wagtail API"""

        ### Arrange ###
        # Create image for test
        img_1 = ImageFactory(file=factory.django.ImageField(width=5000, height=1000))
        img_2 = ImageFactory(file=factory.django.ImageField(width=3000, height=1000))
        img_3 = ImageFactory(file=factory.django.ImageField(width=4000, height=1000))

        # Data for StreamField
        body_data = [
            {
                "type": "headings",
                "value": [
                    {
                        "type": "h1",
                        "value": "fefefezgfefefezgfefefezg",
                    },
                    {
                        "type": "h2",
                        "value": "fefefezgfefefezgfefefezg",
                    }
                ],
            },
            {
                "type": "paragraph",
                "value": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros,  pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus.  Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex, </p>",
            },
            {
                "type": "slider",
                "value": [
                    {
                        "title": "Lorem ipsum dolor sit amet, consectetur adipiscin",
                        "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,</p>",
                        "button_text": "Lorem ipsum",
                        "button": 2,
                        "image": img_1.pk
                    },
                    {
                        "title": "Lorem ipsum dolor sit amet, consectetur adipiscin",
                        "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,</p>",
                        "button_text": "Lorem",
                        "button": 3,
                        "image": img_2.pk
                    },
                    {
                        "title": "Lorem ipsum dolor sit amet, consectetur adipiscin",
                        "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex,</p>",
                        "button_text": "Lorem",
                        "button": 4,
                        "image": img_3.pk
                    }
                ],
            },
        ]

        # Create a flex page who posses body_data 
        flex_page_2 = FlexPageFactory.create(
            parent=self.flex_page, body=json.dumps(body_data)
        )

        ### Act ###
        # Fetch a page from CMS API using flex_page_2 pk
        response = self.client.get(f"/api/cms/pages/{flex_page_2.pk}/")
        response_data = response.json()

        ### Assert ###
        # Test if the fetched page body (StreamField) field data return the correct block
        assert response_data["body"][1]["value"] == body_data[1]["value"]

        # Test the API representation of the flexpages
        assert response_data["body"][2]["type"] == "slider"
        assert response_data["body"][2]["value"][0]["image"]["width"] == 2560



