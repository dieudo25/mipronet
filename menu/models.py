from django.db import models

from django_extensions.db.fields import AutoSlugField
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import (
    MultiFieldPanel,
    InlinePanel,
    FieldPanel,
    PageChooserPanel,
)
from wagtail.api import APIField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.models import Image
from wagtail.core.models import Orderable
from wagtail.snippets.models import register_snippet


class MenuItem(Orderable):
    """
        Menu Item model for the navbar
    """

    link_title = models.CharField(
        blank=True,
        null=True,
        max_length=50
    )
    link_page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    menu = ParentalKey("Menu", related_name="menu_items")

    panels = [
        FieldPanel("link_title"),
        PageChooserPanel("link_page"),
    ]


@register_snippet
class Menu(ClusterableModel):
    """
        The menu clusterable model
    """

    title = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="title", editable=True)
    logo_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    logo_image_sticky = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    logo_text = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="S'affiche uniquement si logo image n'est pas utilisé"
    )
    admin_access = models.URLField(
        verbose_name="Accès à l'administration",
        null=True,
        blank=True,
        max_length=300,
    )
    copyright_text = models.CharField(
        verbose_name="Copyright",
        max_length=100,
        null=True,
        blank=True
    )

    panels = [
        MultiFieldPanel([
            FieldPanel("title"),
            FieldPanel("slug"),
            ImageChooserPanel("logo_image"),
            ImageChooserPanel("logo_image_sticky"),
            FieldPanel("logo_text",),
            FieldPanel("admin_access"),
            FieldPanel("copyright_text"),
        ], heading="Menu"),
        InlinePanel("menu_items", label="Élément du menu")
    ]

    # Add custom property to API
    @property
    def get_menu_items(self):
        return MenuItem.objects.filter(menu=self.pk)

    @property
    def get_menu_logo_image(self):
        return Image.objects.get(id=self.logo_image.pk)

    def __str__(self):
        return self.title
