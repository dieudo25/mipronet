from rest_framework.fields import Field


class MenuItemField(Field):
    """
        Custom MenuItemField for Rest API
    """

    def to_representation(self, menuItems):
        """
            Overwrite to_representation method
            Define how the data is represented in JSON

        """

        try:
            return[
                {
                    "id": item.id,
                    "sort_order": item.sort_order,
                    "link_title": item.link_title,
                    "link_page": item.link_page.slug,
                    "type": f"{item.link_page.content_type.app_label}.{item.link_page.content_type.model}",
                } for item in menuItems.all()
            ]
        except Exception:
            return []
