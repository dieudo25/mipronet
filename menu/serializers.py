from rest_framework import serializers

from .fields import MenuItemField
from .models import Menu, MenuItem

from wagtail.images.models import Image
from wagtail.images.api.fields import ImageRenditionField


class MenuSerializer(serializers.ModelSerializer):
    api_get_menu_items = MenuItemField(source="get_menu_items")

    class Meta:
        model = Menu
        fields = [
            "id",
            "slug",
            "logo_text",
            "admin_access",
            "copyright_text",
            "logo_image",
            "logo_image_sticky",
            "api_get_menu_items",
        ]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.logo_image:
            representation['logo_image'] = ImageRenditionField(
                "width-200").to_representation(instance.logo_image)
        if instance.logo_image_sticky:
            representation['logo_image_sticky'] = ImageRenditionField(
                "width-200").to_representation(instance.logo_image_sticky)
        return representation


class MenuItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItem
        fields = "__all__"
