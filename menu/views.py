from rest_framework import viewsets

from .models import Menu, MenuItem
from .serializers import MenuSerializer, MenuItemSerializer


class MainMenuSet(viewsets.ModelViewSet):
    serializer_class = MenuSerializer
    queryset = Menu.objects.filter(slug="principal")
    http_method_names = ["get"]


class FooterMenuSet(viewsets.ModelViewSet):
    serializer_class = MenuSerializer
    queryset = Menu.objects.filter(slug="footer")
    http_method_names = ["get"]


class MainMenuItemSet(viewsets.ModelViewSet):
    serializer_class = MenuItemSerializer
    queryset = MenuItem.objects.filter(menu__slug="principal")
    http_method_names = ["get"]


class FooterMenuItemSet(viewsets.ModelViewSet):
    serializer_class = MenuItemSerializer
    queryset = MenuItem.objects.filter(menu__slug="footer")
    http_method_names = ["get"]
