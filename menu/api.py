from rest_framework import routers

from .views import (
    MainMenuSet, 
    MainMenuItemSet,
    FooterMenuSet,
    FooterMenuItemSet,
)


# Rest Framework API
menu_router = routers.DefaultRouter()

menu_router.register(r"main/items", MainMenuItemSet)
menu_router.register(r"main", MainMenuSet)
menu_router.register(r"footer/items", FooterMenuItemSet)
menu_router.register(r"footer", FooterMenuSet)
