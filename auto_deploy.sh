#! /bin/bash

# This shell script quickly deploys your project to your
# DigitalOcean Droplet

APP_NAME=MIPRONET
REMOTE_SERVER=$IP_MIPRONET_APP
APP_URL=https://mipronet.cf

if [ -z "$REMOTE_SERVER" ]
then
    echo "REMOTE_SERVER not defined"
    exit 0
fi


printf "######################################################################################\n\n"
printf "Generate TAR file from git...\n\n"
printf "######################################################################################\n\n"

# generate TAR file from git
git archive --format tar --output ./project.tar master

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "TAR COMPRESSION: SUCCESS\n\n"
else
  printf "TAR COMPRESSION: FAILURE\n\n"
  exit 1
fi

printf "######################################################################################\n\n"
printf "Uploading project...\n\n"
printf "######################################################################################\n\n"

rsync ./project.tar root@$REMOTE_SERVER:/tmp/project.tar

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "UPLOADING: SUCCESS\n\n"
else
  printf "UPLOADING: FAILURE\n\n"
  exit 1
fi

printf "######################################################################################\n\n"
printf "Building image...\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh mipronet << 'ENDSSH'
    mkdir -p /app
    rm -rf /app/* && tar -xf /tmp/project.tar -C /app
    docker-compose -f /app/docker-compose.prod.yml build
    supervisorctl restart mipronet-app
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "BUILD: SUCCESS\n\n"
else
  printf "BUILD: FAILURE\n\n"
  exit 1
fi

printf "######################################################################################\n\n"
printf "Migrate Data\n\n"
printf "######################################################################################\n\n"

sleep 5

bash ./compose/scripts/local2prod.sh

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "MIGRATE DATA AND MEDIA FILES: SUCCESS\n\n"
else
  printf "MIGRATE DATA AND MEDIA FILES: FAILURE\n\n"
  exit 1
fi


printf "######################################################################################\n\n"
printf "Config SWAG\n\n"
printf "######################################################################################\n\n"

sleep 10

bash ./compose/scripts/init-swag.sh

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "SWAG CONFIG: SUCCESS\n\n"
else
  printf "SWAG CONFIG: FAILURE\n\n"
  exit 1
fi


printf "######################################################################################\n\n"
printf "DELIVERY OF THE APP $APP_NAME COMPLETE\n\n"
printf "CHECK IT AT - $APP_URL\n\n"
printf "######################################################################################\n\n"