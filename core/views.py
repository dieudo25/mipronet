import os

from django.views.generic import TemplateView

env = os.environ.copy()


class RobotsView(TemplateView):

    content_type = 'text/plain'
    context_object_name = "robots"
    template_name = "core/robots.txt"
