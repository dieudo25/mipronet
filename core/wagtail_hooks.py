from django.templatetags.static import static
from django.utils.html import format_html

import wagtail.admin.rich_text.editors.draftail.features as draftail_features
from wagtail.admin.rich_text.converters.html_to_contentstate import InlineStyleElementHandler
from wagtail.core import hooks


@ hooks.register("insert_global_admin_css", order=100)
def global_admin_css():
    """ Add /static/css/custom.css to the admin css"""

    return format_html('<link rel="stylesheet" href="{}">', static("css/custom_admin.css"))


@ hooks.register("insert_global_admin_js", order=100)
def global_admin_js():
    """ Add /static/js/custom.js to the admin js"""

    return format_html('<script src="{}" defer></script>', static("js/custom_admin.js"))


@ hooks.register("register_rich_text_features")
def register_thin_font_styling(features):
    """ Add to wagtail draftail (rich text editor) custom font style"""

    feature_name = "span"
    type_ = "span"
    tag = "span"

    control = {
        "type": type_,
        "label": "span",
        "description": "span tag",
    }

    features.register_editor_plugin(
        "draftail", feature_name, draftail_features.InlineStyleFeature(control)
    )

    db_conversion = {
        'from_database_format': {tag: InlineStyleElementHandler(type_)},
        'to_database_format': {'style_map': {type_: tag}},
    }

    features.register_converter_rule(
        "contentstate", feature_name, db_conversion)

    # Add feature to all draftail editor by default
    features.default_features.append(feature_name)
