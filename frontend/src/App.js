import React from "react";
import { Route, Switch } from "react-router";
import { Container, Row } from "react-bootstrap";
import { PageContainer } from "./components/pages/PageContainer";

import { AnimatePresence } from "framer-motion";

function App() {
  return (
    <AnimatePresence>
      <Switch>
        <Route path="/:id([\d]+)" component={PageContainer} />
        <Route path="/:html_path([a-z0-9-]+)" component={PageContainer} />
        <Route path="/" exact component={PageContainer} />
        <Route
          path="*"
          component={() => (
            <Container>
              <Row>
                <h1>404</h1>
              </Row>
            </Container>
          )}
        />
      </Switch>
    </AnimatePresence>
  );
}

export default App;
