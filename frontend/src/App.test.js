import React from "react";
import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { within } from "@testing-library/dom";
import { MemoryRouter } from "react-router-dom";

import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import { mockMenu } from "./stories/navigation/mockUtils";
import { mockPageDetail } from "./stories/pages/mockUtils";

import App from "./App";

test("Test Main Menu Link", async () => {
  const mock = new MockAdapter(axios);
  mockMenu(mock);
  mockPageDetail(mock);

  render(
    <MemoryRouter initialEntries={["/"]}>
      <App />
    </MemoryRouter>
  );

  const MainMenuItemElement = screen.getByText("Accueil");
  expect(MainMenuItemElement.tagName).toEqual("a");
  expect(MainMenuItemElement).toHaveClass("nav-link");

  const { getBytText } = within(mainMenuItemElement.parentNode.parentNode);

  await waitFor(() => expect(getBytText("Test")).toBeInTheDocument());

  const el = getBytText("Test");

  fireEvent.click(el);

  await waitFor(() =>
    expect(screen.getBytText("Slider 12")).toBeInTheDocument()
  );
});
