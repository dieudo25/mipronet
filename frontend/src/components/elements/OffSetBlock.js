import React from "react";

import "../../style/index.scss";

function OffSetBlock(props) {
  let id = props.value.block_id;
  let class_style = props.value.class_style;

  return <div id={id ? id : ""} className={`${class_style}`}></div>;
}

export { OffSetBlock };
