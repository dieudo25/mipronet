import React from "react";
import { Spinner } from "react-bootstrap";

class CustomSpinner extends React.Component {
  render() {
    return (
      <div className="spinner-container">
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </div>
    );
  }
}

export { CustomSpinner };
