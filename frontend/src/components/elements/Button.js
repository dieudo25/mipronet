import React from "react";

import { Link } from "react-router-dom";
import "../../style/index.scss";

function Button(props) {
  if (props.cmsAdmin) {
    return (
      <a className={`button-primary`} href={props.cmsAdmin}>
        {props.buttonText}
      </a>
    );
  } else if (props.streamfield && props.value.external_url) {
    return (
      <div className={`${props.value.style_classes}`}>
        <Link
          className={`button-${props.value.style_type}`}
          to={props.value.external_url}
        >
          {props.value.text}
        </Link>
      </div>
    );
  } else if (props.streamfield) {
    const slug = props.value.page.slug;
    const title = props.value.page.title;
    const content_type = props.value.page.content_type;
    const style_classes = props.value.style_classes;
    const style_type = props.value.style_type;
    return (
      <div className={`${style_classes}`}>
        <Link
          className={`button-${style_type}`}
          to={content_type === "flex.homepage" ? "/" : `/${slug}`}
        >
          {title}
        </Link>
      </div>
    );
  } else {
    return (
      <Link
        className={`button-primary`}
        to={props.contentType === "flex.homepage" ? "/" : `/${props.slug}`}
      >
        {props.buttonText}
      </Link>
    );
  }
}

export { Button };
