import React from "react";
import Fade from "react-reveal/Fade";

function Heading(props) {
  const animation = props.value.animation.reveal;
  const animationBig = props.value.animation.reveal_big;
  const animationCascade = props.value.animation.reveal_cascade;

  if (animation) {
    let top = false;
    let bottom = false;
    let left = false;
    let right = false;
    let big = false;
    let cascade = false;

    switch (animation) {
      case "top":
        top = true;
        break;
      case "bottom":
        bottom = true;
        break;
      case "left":
        left = true;
        break;
      case "right":
        right = true;
        break;
      default:
        break;
    }
    if (animationBig) {
      big = true;
    }
    if (animationCascade) {
      cascade = true;
    }
    switch (props.value.heading_type) {
      case "h1":
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h1>{props.value.text}</h1>
            </div>
          </Fade>
        );
      case "h2":
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h2>{props.value.text}</h2>
            </div>
          </Fade>
        );
      case "h3":
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h3>{props.value.text}</h3>
            </div>
          </Fade>
        );
      case "h4":
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h4>{props.value.text}</h4>
            </div>
          </Fade>
        );
      case "h5":
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h5>{props.value.text}</h5>
            </div>
          </Fade>
        );

      default:
        return (
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={props.value.style_classes}>
              <h6>{props.value.text}</h6>
            </div>
          </Fade>
        );
    }
  } else {
    switch (props.value.heading_type) {
      case "h1":
        return (
          <div className={props.value.style_classes}>
            <h1>{props.value.text}</h1>
          </div>
        );
      case "h2":
        return (
          <div className={props.value.style_classes}>
            <h2>{props.value.text}</h2>
          </div>
        );
      case "h3":
        return (
          <div className={props.value.style_classes}>
            <h3>{props.value.text}</h3>
          </div>
        );
      case "h4":
        return (
          <div className={props.value.style_classes}>
            <h4>{props.value.text}</h4>
          </div>
        );
      case "h5":
        return (
          <div className={props.value.style_classes}>
            <h5>{props.value.text}</h5>
          </div>
        );
      case "h6":
        return (
          <div className={props.value.style_classes}>
            <h6>{props.value.text}</h6>
          </div>
        );
      default:
        return <div className="d-none"></div>;
    }
  }
}

export { Heading };
