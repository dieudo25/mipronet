import React from "react";

import axios from "axios";

import { MainMenu } from "../navigation/MainMenu";
import { FooterMenu } from "../navigation/FooterMenu";
import { PageDetail } from "./PageDetail";
import { CustomSpinner } from "../elements/Spinner";

class PageContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
      footer: [],
      page: [],
      socialSidebar: null,
      loading: true,
    };
  }

  componentDidMount() {
    const html_path = this.props.match.params.html_path;
    const pk = this.props.match.params.id;

    // convert querystring to dict fro preview page
    const querystring = this.props.location.search.replace(/^\?/, "");
    const params = {};
    querystring.replace(/([^=&]+)=([^&]*)/g, function (m, key, value) {
      params[decodeURIComponent(key)] = decodeURIComponent(value);
    });

    // URL of API to fetch data
    let mainMenuURL = "/api/menu/main/";
    let footerMenuURL = "/api/menu/footer/";
    let socialSidebar = "/api/social/sidebar/";
    let socialFooter = "/api/social/footer/";
    let detailPageURLHtmlPath = `/api/cms/pages/find/?html_path=${html_path}`;
    let detailHomePageURL = `/api/cms/pages/find/?html_path=/`;
    let detailPageURLPK = `/api/cms/pages/${pk}/`;
    let detailPagePreviewURL = `/api/cms/page_preview/1/${this.props.location.search}`;

    // If page preview
    if (params.token) {
      axios
        .all([
          axios.get(mainMenuURL),
          axios.get(footerMenuURL),
          axios.get(detailPagePreviewURL),
          axios.get(socialSidebar),
          axios.get(socialFooter),
        ])
        .then(
          axios.spread((...responses) => {
            const menu = responses[0].data.results[0];
            const footer = responses[1].data.results[0];
            const detailPagePreview = responses[2].data;
            const socialSidebar = responses[3].data;
            const socialFooter = responses[4].data;

            this.setState({
              menu: menu,
              footer: footer,
              page: detailPagePreview,
              socialSidebar: socialSidebar,
              socialFooter: socialFooter,
              loading: false,
            });
          })
        )
        .catch((error) => {
          console.log(error);
        });
    } else {
      // If normal page
      if (html_path) {
        // fetch data using slug
        axios
          .all([
            axios.get(mainMenuURL),
            axios.get(footerMenuURL),
            axios.get(detailPageURLHtmlPath),
            axios.get(socialSidebar),
            axios.get(socialFooter),
          ])
          .then(
            axios.spread((...responses) => {
              const menu = responses[0].data.results[0];
              const footer = responses[1].data.results[0];
              const detailPage = responses[2].data;
              const socialSidebar = responses[3].data;
              const socialFooter = responses[4].data;

              this.setState({
                menu: menu,
                footer: footer,
                page: detailPage,
                socialSidebar: socialSidebar,
                socialFooter: socialFooter,
                loading: false,
              });
            })
          )
          .catch((error) => {
            console.log(error);
          });
      } else if (pk) {
        // Fetch data using pk
        axios
          .all([
            axios.get(mainMenuURL),
            axios.get(footerMenuURL),
            axios.get(detailPageURLPK),
            axios.get(socialSidebar),
            axios.get(socialFooter),
          ])
          .then(
            axios.spread((...responses) => {
              const menu = responses[0].data.results[0];
              const footer = responses[1].data.results[0];
              const detailPage = responses[2].data;
              const socialSidebar = responses[3].data;
              const socialFooter = responses[4].data;

              this.setState({
                menu: menu,
                footer: footer,
                page: detailPage,
                socialSidebar: socialSidebar,
                socialFooter: socialFooter,
                loading: false,
              });
            })
          )
          .catch((error) => {
            console.log(error);
          });
      } else {
        // Fetch home page data as default
        axios
          .all([
            axios.get(mainMenuURL),
            axios.get(footerMenuURL),
            axios.get(detailHomePageURL),
            axios.get(socialSidebar),
            axios.get(socialFooter),
          ])
          .then(
            axios.spread((...responses) => {
              const menu = responses[0].data.results[0];
              const footer = responses[1].data.results[0];
              const detailPage = responses[2].data;
              const socialSidebar = responses[3].data;
              const socialFooter = responses[4].data;

              this.setState({
                menu: menu,
                footer: footer,
                page: detailPage,
                socialSidebar: socialSidebar,
                socialFooter: socialFooter,
                loading: false,
              });
            })
          )
          .catch((error) => {
            console.log(error);
          });
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (
      // if there is a change of page (location) between the prevState and intance state
      this.props.match.params.html_path !== prevProps.match.params.html_path
    ) {
      const html_path = this.props.match.params.html_path;

      let mainMenuURL = "/api/menu/main/";
      let footerMenuURL = "/api/menu/footer/";
      let socialSidebar = "/api/social/sidebar/";
      let socialFooter = "/api/social/footer/";
      let detailPageURLHtmlPath = `/api/cms/pages/find/?html_path=${html_path}`;
      let detailHomePageURL = `/api/cms/pages/find/?html_path=/`;

      // Get data from API
      if (html_path) {
        // Using slug
        axios
          .all([
            axios.get(mainMenuURL),
            axios.get(footerMenuURL),
            axios.get(detailPageURLHtmlPath),
            axios.get(socialSidebar),
            axios.get(socialFooter),
          ])
          .then(
            axios.spread((...responses) => {
              const menu = responses[0].data.results[0];
              const footer = responses[1].data.results[0];
              const detailPage = responses[2].data;
              const socialSidebar = responses[3].data;
              const socialFooter = responses[4].data;

              this.setState({
                menu: menu,
                footer: footer,
                page: detailPage,
                socialSidebar: socialSidebar,
                socialFooter: socialFooter,
                loading: false,
              });

              // If change of locaiton -> define scroll to the top of the page
              if (this.props.location !== prevProps.location) {
                window.scrollTo(0, 0);
              }
            })
          )
          .catch((error) => {
            console.log(error);
          });
      } else {
        // Fetch data for homepage as default
        axios
          .all([
            axios.get(mainMenuURL),
            axios.get(footerMenuURL),
            axios.get(detailHomePageURL),
            axios.get(socialSidebar),
            axios.get(socialFooter),
          ])
          .then(
            // If change of locaiton -> define scroll to the top of the page
            axios.spread((...responses) => {
              const menu = responses[0].data.results[0];
              const footer = responses[1].data.results[0];
              const detailPage = responses[2].data;
              const socialSidebar = responses[3].data;
              const socialFooter = responses[4].data;

              this.setState({
                menu: menu,
                footer: footer,
                page: detailPage,
                socialSidebar: socialSidebar,
                socialFooter: socialFooter,
                loading: false,
              });

              // If change of locaiton -> define scroll to the top of the page
              if (this.props.location !== prevProps.location) {
                window.scrollTo(0, 0);
              }
            })
          )
          .catch((error) => {
            console.log(error);
          });
      }
    }
  }

  render() {
    if (!this.state.loading) {
      return (
        <>
          <MainMenu menu={this.state.menu} social={this.state.socialSidebar} />
          <main>
            <article>
              <PageDetail {...this.props} page={this.state.page} />
            </article>
          </main>
          <FooterMenu
            footer={this.state.footer}
            social={this.state.socialFooter}
          />
        </>
      );
    } else {
      return <CustomSpinner />;
    }
  }
}

export { PageContainer };
