import React from "react";
import { Helmet } from "react-helmet";
import { StreamField } from "../streamfield/StreamField";

import { motion } from "framer-motion";

class PageDetail extends React.Component {
  render() {
    const page = this.props.page;
    const meta = page.meta;
    const title = page.title;
    const keywords = page.keywords;
    const description = page.description;

    const pageTransition = {
      in: {},
      out: {},
    };

    return (
      <>
        <Helmet>
          <html lang="fr" />
          <title>{meta.seo_title ? meta.seo_title : title}</title>
          <meta name="keywords" content={keywords} />
          <meta
            name="description"
            content={
              meta.search_description ? meta.search_description : description
            }
          />
        </Helmet>
        <motion.div
          initial="out"
          animate="in"
          exit="out"
          variants={pageTransition}
          className={`page-${page.meta.slug}`}
        >
          <StreamField value={page.body} />
        </motion.div>
      </>
    );
  }
}

export { PageDetail };
