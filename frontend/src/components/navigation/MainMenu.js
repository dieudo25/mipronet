import React from "react";

import "../../style/index.scss";

import { Logo } from "./MainLogo";
import { MenuItemList } from "./MainMenuItemList";

class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggleButtonClickHandle = this.handleToggleButtonClickHandle.bind(
      this
    );
    this.handleCloseButtonClickHandle = this.handleCloseButtonClickHandle.bind(
      this
    );
    this.handleScroll = this.handleScroll.bind(this);

    this.state = {
      stickyMenu: false,
      toggle: false,
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", () => this.handleScroll);
  }

  handleScroll() {
    if (window.pageYOffset > 140) {
      this.setState({ stickyMenu: true });
    } else {
      this.setState({ stickyMenu: false });
    }
  }

  handleToggleButtonClickHandle() {
    this.setState({
      toggle: !this.state.toggle,
    });
  }

  handleCloseButtonClickHandle() {
    this.setState({
      toggle: false,
    });
  }

  render() {
    const menuItems = this.props.menu.api_get_menu_items;
    const menuLogoText = this.props.menu.logo_text;
    const menuLogoImage = this.props.menu.logo_image;
    const menuLogoImageSticky = this.props.menu.logo_image_sticky;
    const toggle = this.state.toggle;
    const sticky = this.state.stickyMenu;
    const social = this.props.social.results[0].api_get_social_items;
    return (
      <nav className={`main-nav ${sticky ? "sticky-nav" : ""}`}>
        <div className="main-nav-container">
          <div className="logo-container">
            <Logo
              inSideBar={false}
              logoText={menuLogoText}
              logoImageSticky={menuLogoImageSticky}
              logoImage={menuLogoImage}
              sticky={sticky}
            />
          </div>
          <button
            className="main-nav-toggle"
            onClick={this.handleToggleButtonClickHandle}
          >
            <span />
            <span />
            <span />
          </button>
          <div className={`${toggle ? "active" : ""} main-nav-menu`}>
            <div className="logo-container">
              <Logo
                centered={true}
                logoText={menuLogoText}
                logoImage={menuLogoImageSticky}
                inSideBar={true}
                canCloseNavMenu={true}
                handleCloseButtonClickHandle={this.handleCloseButtonClickHandle}
              />
            </div>
            <MenuItemList
              itemList={menuItems}
              inSideBar={true}
              handleCloseButtonClickHandle={this.handleCloseButtonClickHandle}
            />
            <ul className="social-network">
              {social.map((item) => (
                <li key={item.id}>
                  <a href={item.link_url}>
                    <img
                      src={item.logo_image.url}
                      width={item.logo_image.width}
                      height={item.logo_image.height}
                      alt={item.logo_image.alt}
                    ></img>
                  </a>
                </li>
              ))}
            </ul>
          </div>
          <div
            onClick={this.handleCloseButtonClickHandle}
            className={`close-sidebar ${toggle ? "active" : ""}`}
          />
        </div>
      </nav>
    );
  }
}

export { MainMenu };
