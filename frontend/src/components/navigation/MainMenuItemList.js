import React from "react";

import { Link } from "react-router-dom";

import "../../style/index.scss";

class MenuItemList extends React.Component {
  constructor(props) {
    super(props);
    this.handleCloseButtonClickHandle = this.handleCloseButtonClickHandle.bind(
      this
    );
  }

  handleCloseButtonClickHandle() {
    this.props.handleCloseButtonClickHandle();
  }

  render() {
    return (
      <ul className="main-nav-item-list">
        {this.props.itemList.map((item) => (
          <li key={item.id}>
            <Link
              className="nav-link"
              to={item.type === "flex.homepage" ? "/" : `/${item.link_page}`}
              onClick={this.handleCloseButtonClickHandle}
            >
              {item.link_title}
            </Link>
          </li>
        ))}
      </ul>
    );
  }
}

export { MenuItemList };
