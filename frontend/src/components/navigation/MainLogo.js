import React from "react";

import { Link } from "react-router-dom";

import "../../style/index.scss";

class Logo extends React.Component {
  constructor(props) {
    super(props);
    this.handleCloseButtonClickHandle = this.handleCloseButtonClickHandle.bind(
      this
    );
  }

  handleCloseButtonClickHandle() {
    this.props.handleCloseButtonClickHandle();
  }

  render() {
    let sticky = this.props.sticky;
    if (this.props.logoImage && this.props.canCloseNavMenu) {
      return (
        <Link
          to="/"
          className="main-nav-logo"
          onClick={this.handleCloseButtonClickHandle}
        >
          <img
            src={
              sticky ? this.props.logoImageSticky.url : this.props.logoImage.url
            }
            alt={this.props.logoImage.alt}
          ></img>
        </Link>
      );
    } else if (this.props.logoImage) {
      return (
        <Link to="/" className="main-nav-logo">
          <img
            src={
              sticky ? this.props.logoImageSticky.url : this.props.logoImage.url
            }
            alt={this.props.logoImage.alt}
          ></img>
        </Link>
      );
    } else {
      return (
        <Link to="/" className="main-nav-logo">
          {this.props.logoText}
        </Link>
      );
    }
  }
}

export { Logo };
