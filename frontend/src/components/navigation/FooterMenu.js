import React from "react";

import "../../style/index.scss";

import { MenuItemList } from "./FooterMenuItemList";
import { Logo } from "./FooterLogo";

class FooterMenu extends React.Component {
  render() {
    const footerItems = this.props.footer.api_get_menu_items;
    const logoText = this.props.footer.logo_text;
    const logoImage = this.props.footer.logo_image;
    const copyRightText = this.props.footer.copyright_text;
    const social = this.props.social.results[0].api_get_social_items;

    return (
      <footer>
        <div className="footer-container">
          <Logo logoText={logoText} logoImage={logoImage} />
          <div className="footer-nav-menu">
            <MenuItemList itemList={footerItems} inSideBar={false} />
          </div>
          <ul className="social-network">
            {social.map((item) => (
              <li key={item.id}>
                <a href={item.link_url}>
                  <img
                    src={item.logo_image.url}
                    width={item.logo_image.width}
                    height={item.logo_image.height}
                    alt={item.logo_image.alt}
                  ></img>
                </a>
              </li>
            ))}
          </ul>
        </div>
        <p className="copyright-text">{copyRightText}</p>
      </footer>
    );
  }
}

export { FooterMenu };
