import React from "react";

import "../../style/index.scss";
import { Link } from "react-router-dom";

class MenuItemList extends React.Component {
  render() {
    return (
      <ul className="footer-item-list">
        {this.props.itemList.map((item) => (
          <li key={item.id}>
            <Link
              to={item.type === "flex.homepage" ? "/" : `/${item.link_page}`}
            >
              {item.link_title}
            </Link>
          </li>
        ))}
      </ul>
    );
  }
}

export { MenuItemList };
