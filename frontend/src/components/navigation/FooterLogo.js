import React from "react";

import "../../style/index.scss";
import { Link } from "react-router-dom";

class Logo extends React.Component {
  render() {
    if (this.props.logoImage) {
      return (
        <Link to="/" className="footer-logo">
          <img
            src={this.props.logoImage.url}
            alt={this.props.logoImage.alt}
          ></img>
        </Link>
      );
    } else {
      return (
        <Link to="/" className="footer-logo">
          {this.props.logoText}
        </Link>
      );
    }
  }
}

export { Logo };
