import React from "react";

import { sanitize } from "dompurify";

import { Button } from "../elements/Button";

function FullScreenSlider(props) {
  const image = props.value.image;
  const title = props.value.title;
  const description = props.value.description;
  const button = props.value.button;

  if (button) {
    const slug = props.value.button.slug;
    const contentType = props.value.button.content_type;
    const button_text = props.value.button_text;

    return (
      <div className="fullscreen-slide-item">
        <div className="fullscreen-img-container">
          <img src={image.url} alt={image.url}></img>
        </div>
        <div className="fullscreen-text">
          <h2>{title}</h2>
          <div
            className="fullscreen-paraph"
            dangerouslySetInnerHTML={{
              __html: `${sanitize(description)}`,
            }}
          />
          <div className="banner-btn">
            <Button
              contentType={contentType}
              slug={slug}
              buttonText={button_text}
            />
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="fullscreen-slide-item">
        <div className="fullscreen-img-container">
          <img src={image.url} alt={image.url}></img>
        </div>
        <div className="fullscreen-text">
          <h2>{title}</h2>
          <div
            className="fullscreen-paraph"
            dangerouslySetInnerHTML={{
              __html: `${sanitize(description)}`,
            }}
          />
        </div>
      </div>
    );
  }
}

export { FullScreenSlider };
