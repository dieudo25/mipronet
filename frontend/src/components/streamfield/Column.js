import React from "react";

import "../../style/index.scss";
import { StreamField } from "./StreamField";

function TwoColumn(props) {
  let id = props.value.id;
  return (
    <div
      id={id ? id : ""}
      className={`column-container ${props.value.container_classes}`}
    >
      <div className={`column two-col ${props.value.classes} `}>
        <StreamField value={props.value.column} col={2} />
      </div>
    </div>
  );
}

function ThreeColumn(props) {
  return (
    <div className="column-container">
      <div className="column three-col">
        <StreamField value={props.value} col={3} />
      </div>
    </div>
  );
}

function FourColumn(props) {
  return (
    <div className={`column-container ${props.value.container_classes}`}>
      <div className={`column four-col ${props.value.classes} `}>
        <StreamField value={props.value} col={4} />
      </div>
    </div>
  );
}

export { TwoColumn, ThreeColumn, FourColumn };
