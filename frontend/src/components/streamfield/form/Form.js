import React from "react";
import axios from "axios";
import { Helmet } from "react-helmet";
import Fade from "react-reveal/Fade";

import { slugify } from "../../../utils/String";

import { StreamField } from "./StreamFieldForm";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormChange = this.handleFormChange.bind(this);

    this.state = {
      fields: {},
      errors: {},
      message: [],
      submitState: "waiting for submit",
      reCaptchaState: "waiting for checked",
    };
  }

  getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== "") {
      let cookies = document.cookie.split(";");
      for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i].trim();
        if (cookie.substring(0, name.length + 1) === name + "=") {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  handleFormValidation = () => {
    let state_fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    for (let [key, value] of Object.entries(state_fields)) {
      switch (key) {
        case "telephone":
          if (typeof value !== "undefined") {
            if (
              !value.match(
                /^(?:(?:\+|00)\d{2}[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/
              )
            ) {
              formIsValid = false;
              errors[key] = "Champ non valide";
            }
          }
          break;
        case "email":
          if (typeof value !== "undefined") {
            if (
              !value.match(
                /^(([^<>()[\]\\.,;:\s@]+(\.[^<>()[\]\\.,;:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
              )
            ) {
              formIsValid = false;
              errors[key] = "Champ non valide";
            }
          }
          break;
        case "message":
          break;

        case "entreprise":
          if (typeof value !== "undefined") {
            if (!value.match(/^.+$/)) {
              formIsValid = false;
              errors[key] = "Champ non valide";
            }
          }
          break;
        default:
          if (typeof value !== "undefined") {
            if (!value.match(/^[a-z\u00E0-\u00FC]+$/i)) {
              formIsValid = false;
              errors[key] = "Champ non valide";
            }
          }
      }

      if (!value) {
        formIsValid = false;
        errors[key] = "Ce champ est obligatoire";
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  };

  clearForm = () => {
    let fields = this.props.value.form.fields;
    let state_fields = {};

    fields.forEach((field) => {
      let name = slugify(field.value.label);
      state_fields[name] = "";
    });

    this.setState({
      fields: state_fields,
    });
  };

  handleFormChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    let state_fields = this.state.fields;

    state_fields[name] = value;

    this.setState({
      fields: state_fields,
    });
  };

  handleFormSubmit = (event) => {
    event.preventDefault();

    let csrftoken = this.getCookie("csrftoken");

    if (this.handleFormValidation()) {
      this.setState({
        message: this.props.value.form.success_message,
        submitState: "success",
      });
      console.log(JSON.stringify(this.state.fields));

      axios({
        method: "post",
        url: "/api/form/submissons/",
        data: {
          form_data: JSON.stringify(this.state.fields),
          form: this.props.value.form.id,
        },
        headers: {
          "X-CSRFToken": csrftoken,
          "Content-Type": "application/json",
        },
      });

      this.clearForm();
    } else {
      this.setState({
        message: this.props.value.form.error_message,
        submitState: "failure",
      });
    }
  };

  componentDidMount() {
    let fields = this.props.value.form.fields;
    let state_fields = {};

    fields.forEach((field) => {
      let name = slugify(field.value.label);
      let value = field.value.default_value;
      state_fields[name] = value;

      this.setState({
        fields: state_fields,
      });
    });
  }

  onloadCallback = (response) => {
    console.log(response);
    this.setState({ reCaptchaState: "success" });
  };

  render() {
    let fields = this.props.value.form.fields;
    let formSlug = this.props.value.form.slug;
    let submitButtonText = this.props.value.form.submit_button_text;
    let messageClass = " d-none";
    let formButtonClass = "";

    let animation = this.props.value.animation.reveal;
    let animationBig = this.props.value.animation.reveal_big;
    let animationCascade = this.props.value.animation.reveal_cascade;

    switch (this.state.submitState) {
      case "success":
        messageClass = "success";
        break;
      case "failure":
        messageClass = "danger";
        break;
      default:
        messageClass = " d-none";
    }

    switch (this.state.reCaptchaState) {
      case "success":
        formButtonClass = "";
        break;
      default:
        formButtonClass = "";
        break;
    }

    if (animation) {
      let top = false;
      let bottom = false;
      let left = false;
      let right = false;
      let big = false;
      let cascade = false;

      switch (animation) {
        case "top":
          top = true;
          break;
        case "bottom":
          bottom = true;
          break;
        case "left":
          left = true;
          break;
        case "right":
          right = true;
          break;
        default:
          break;
      }
      if (animationBig) {
        big = true;
      }

      if (animationCascade) {
        cascade = true;
      }

      return (
        <Fade
          top={top}
          bottom={bottom}
          left={left}
          right={right}
          big={big}
          cascade={cascade}
        >
          <Helmet>
            <script
              src="https://www.google.com/recaptcha/api.js"
              async
              defer
            ></script>
          </Helmet>
          <div className="form-container">
            <div className={`form-message alert alert-${messageClass}`}>
              {this.state.message}
            </div>
            <form
              id={formSlug}
              onSubmit={this.handleFormSubmit}
              noValidate="novalidate"
            >
              <StreamField
                value={fields}
                fields={this.state.fields}
                errors={this.state.errors}
                handleFormChange={this.handleFormChange}
              />
            </form>

            {/* {reCaptcha ? (
              <Recaptcha
                sitekey={api_key}
                onloadCallback={this.onloadCallback}
              />
            ) : (
              reCaptcha
            )} */}

            <button
              type="submit"
              form={formSlug}
              className={`form-submit button-primary ${formButtonClass}`}
            >
              {submitButtonText}
            </button>
          </div>
        </Fade>
      );
    } else {
      return (
        <>
          <Helmet>
            <script
              src="https://www.google.com/recaptcha/api.js"
              async
              defer
            ></script>
          </Helmet>
          <div className="form-container">
            <div className={`form-message alert alert-${messageClass}`}>
              {this.state.message}
            </div>
            <form
              id={formSlug}
              onSubmit={this.handleFormSubmit}
              noValidate="novalidate"
            >
              <StreamField
                value={fields}
                fields={this.state.fields}
                errors={this.state.errors}
                handleFormChange={this.handleFormChange}
              />
            </form>

            {/* {reCaptcha ? (
              <Recaptcha
                sitekey={api_key}
                onloadCallback={this.onloadCallback}
              />
            ) : (
              reCaptcha
            )} */}

            <button
              type="submit"
              form={formSlug}
              className={`form-submit button-primary ${formButtonClass}`}
            >
              {submitButtonText}
            </button>
          </div>
        </>
      );
    }
  }
}

export { Form };
