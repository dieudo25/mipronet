import React from "react";

import { slugify } from "../../../utils/String";

import { CharField, EmailField, TextAreaField } from "./Field";

class StreamField extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormChange = this.handleFormChange.bind(this);
  }

  handleFormChange = (event) => {
    this.props.handleFormChange(event);
  };

  render() {
    // We build a StreamField Component, which iterate the props.value
    let streamField = this.props.value;
    let html = [];

    for (let i = 0; i < streamField.length; i++) {
      let field = streamField[i];

      // Decide which block component should be used according to the block type
      switch (field.type) {
        case "singleline": {
          let name = slugify(field.value.label);
          html.push(
            <CharField
              value={field.value}
              field_value={this.props.fields[name]}
              error={this.props.errors[name]}
              key={`${i}.${field.type}`}
              handleFormChange={this.handleFormChange}
            />
          );
          break;
        }
        case "email": {
          let name = slugify(field.value.label);
          html.push(
            <EmailField
              value={field.value}
              field_value={this.props.fields[name]}
              error={this.props.errors[name]}
              key={`${i}.${field.type}`}
              handleFormChange={this.handleFormChange}
            />
          );
          break;
        }
        case "multiline": {
          let name = slugify(field.value.label);
          html.push(
            <TextAreaField
              value={field.value}
              field_value={this.props.fields[name]}
              error={this.props.errors[name]}
              key={`${i}.${field.type}`}
              handleFormChange={this.handleFormChange}
            />
          );
          break;
        }
        default:
          html.push(
            <div value={field.value} key={`${i}.${field.type}`}>
              <h2 className="text-danger">Error for {field.type}</h2>
            </div>
          );
      }
    }
    return html;
  }
}

export { StreamField };
