import React from "react";
import { slugify } from "../../../utils/String";

class CharField extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormChange = this.handleFormChange.bind(this);
  }

  handleFormChange = (event) => {
    this.props.handleFormChange(event);
  };

  render() {
    let label = this.props.value.label;
    let name = slugify(label);
    let value = this.props.field_value;
    let error = this.props.error;

    return (
      <div className={`field field-singleline field-${name}`}>
        <label for={name}>{label} :</label>
        <input
          type="text"
          value={value}
          name={name}
          onChange={this.handleFormChange}
          required={this.props.value.required ? "required" : ""}
        />
        <span className="error-message">{error}</span>
      </div>
    );
  }
}

class EmailField extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormChange = this.handleFormChange.bind(this);
  }

  handleFormChange = (event) => {
    this.props.handleFormChange(event);
  };

  render() {
    let label = this.props.value.label;
    let name = slugify(label);
    let value = this.props.field_value;
    let error = this.props.error;

    return (
      <div className={`field field-email-1 field-${name}`}>
        <label for={name}>{label} :</label>
        <input
          type="email"
          value={value}
          name={name}
          onChange={this.handleFormChange}
          required={this.props.value.required ? "required" : ""}
        />
        <span className="error-message">{error}</span>
      </div>
    );
  }
}

class TextAreaField extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormChange = this.handleFormChange.bind(this);
  }

  handleFormChange = (event) => {
    this.props.handleFormChange(event);
  };

  render() {
    let label = this.props.value.label;
    let name = slugify(label);
    let value = this.props.field_value;
    let error = this.props.error;

    return (
      <div className={`field field-text-area field-${name}`}>
        <label for={name}>{label} :</label>
        <textarea
          value={value}
          name={name}
          onChange={this.handleFormChange}
          required={this.props.value.required ? "required" : ""}
        />
        <span className="error-message">{error}</span>
      </div>
    );
  }
}

export { CharField, EmailField, TextAreaField };
