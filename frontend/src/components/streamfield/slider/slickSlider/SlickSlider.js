// You can live edit this code below the import statements
import React from "react";

import "./_slick.scss";
import "./_slickTheme.scss";

import Fade from "react-reveal/Fade";
import { StreamField } from "../../StreamField";
import { Heading } from "../../../elements/headings";
import { Button } from "../../../elements/Button";

import Slider from "react-slick";

function SlickSlider(props) {
  let slick_settings = props.value.settings;
  let slick_responsive_settings = props.value.responsive_settings;
  let styleClasses = props.value.style_classes;
  let title = props.value.title;
  let button = props.value.button;
  let responsive = [];
  slick_responsive_settings.forEach((setting) => {
    let settingData = {
      breakpoint: setting.breakpoints,
      settings: {
        slidesToShow: setting.slideToShow,
        slidesToScroll: setting.slideToScroll,
        vertical: setting.vertical,
        verticalSwiping: setting.vertical_swiping,
        autoplay: setting.autoplay,
        speed: setting.speed,
        arrows: setting.arrows,
        dots: setting.dots,
        fade: setting.fade,
        infinite: setting.infinite,
        pauseOnFocus: setting.pause_on_focus,
        pauseOnHover: setting.pause_on_hover,
        pauseOnDotsHover: setting.pause_on_dots_hover,
      },
    };

    responsive.push(settingData);
  });

  let settings = {
    autoplay: slick_settings.autoplay,
    speed: slick_settings.speed,
    arrows: slick_settings.arrows,
    dots: slick_settings.dots,
    fade: slick_settings.fade,
    infinite: slick_settings.infinite,
    pauseOnFocus: slick_settings.pause_on_focus,
    pauseOnHover: slick_settings.pause_on_hover,
    pauseOnDotsHover: slick_settings.pause_on_dots_hover,
    slidesToShow: slick_settings.slideToShow,
    slideToScroll: slick_settings.slideToScroll,
    vertical: slick_settings.vertical,
    verticalSwiping: slick_settings.vertical_swiping,
    responsive: responsive,
  };

  let animation = props.value.animation.reveal;
  let animationBig = props.value.animation.reveal_big;
  let animationCascade = props.value.animation.reveal_cascade;

  let top = false;
  let bottom = false;
  let left = false;
  let right = false;
  let big = false;
  let cascade = false;

  if (animation) {
    switch (animation) {
      case "top":
        top = true;
        break;
      case "bottom":
        bottom = true;
        break;
      case "left":
        left = true;
        break;
      case "right":
        right = true;
        break;
      default:
        break;
    }
    if (animationBig) {
      big = true;
    }
    if (animationCascade) {
      cascade = true;
    }
  }

  if (button.page && animation) {
    return (
      <Fade
        top={top}
        bottom={bottom}
        left={left}
        right={right}
        big={big}
        cascade={cascade}
      >
        <div>
          <Heading value={title} />
          <Slider className={styleClasses} {...settings}>
            {props.value.items.map((item, index) => (
              <StreamField value={Array(item)} key={item.type + "." + index} />
            ))}
          </Slider>
          <Button value={button} streamfield={true} />
        </div>
      </Fade>
    );
  } else if (animation) {
    return (
      <Fade
        top={top}
        bottom={bottom}
        left={left}
        right={right}
        big={big}
        cascade={cascade}
      >
        <div>
          <Heading value={title} />
          <Slider className={styleClasses} {...settings}>
            {props.value.items.map((item, index) => (
              <StreamField value={Array(item)} key={item.type + "." + index} />
            ))}
          </Slider>
        </div>
      </Fade>
    );
  } else if (button.page) {
    return (
      <div>
        <Heading value={title} />
        <Slider className={styleClasses} {...settings}>
          {props.value.items.map((item, index) => (
            <StreamField value={Array(item)} key={item.type + "." + index} />
          ))}
        </Slider>
        <Button value={button} streamfield={true} />
      </div>
    );
  } else {
    return (
      <div>
        <Heading value={title} />
        <Slider className={styleClasses} {...settings}>
          {props.value.items.map((item, index) => (
            <StreamField value={Array(item)} key={item.type + "." + index} />
          ))}
        </Slider>
      </div>
    );
  }
}

export { SlickSlider };
