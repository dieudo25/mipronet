import React from "react";
import { sanitize } from "dompurify";
import { HashLink } from "react-router-hash-link";
import Fade from "react-reveal/Fade";

import "../../style/index.scss";

function Card(props) {
  let pageLink = props.value.page_link;
  let anchor = props.value.anchor;
  let styleClass = props.value.style_class;
  let image = props.value.image;
  let text = props.value.text;
  let display = props.value.display;
  let animation = props.value.animation.reveal;
  let animationBig = props.value.animation.reveal_big;
  let animationCascade = props.value.animation.reveal_cascade;

  if (animation) {
    let top = false;
    let bottom = false;
    let left = false;
    let right = false;
    let big = false;
    let cascade = false;

    switch (animation) {
      case "top":
        top = true;
        break;
      case "bottom":
        bottom = true;
        break;
      case "left":
        left = true;
        break;
      case "right":
        right = true;
        break;
      default:
        break;
    }
    if (animationBig) {
      big = true;
    }
    if (animationCascade) {
      cascade = true;
    }

    if (pageLink) {
      return (
        <Fade
          top={top}
          bottom={bottom}
          left={left}
          right={right}
          big={big}
          cascade={cascade}
        >
          <div className={`card-container`}>
            <HashLink
              smooth
              to={
                pageLink.content_type === "flex.homepage"
                  ? "/"
                  : `/${pageLink.slug}#${anchor}`
              }
            >
              <div
                className={`card-link card-body shadow ${display} ${styleClass}`}
              >
                <div className="card-image">
                  <img
                    src={image.url}
                    alt={image.alt}
                    width={image.width}
                    height={image.height}
                  />
                </div>
                <div className="card-text">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: `${sanitize(props.value.text)}`,
                    }}
                  />
                </div>
              </div>
            </HashLink>
          </div>
        </Fade>
      );
    } else {
      return (
        <Fade
          top={top}
          bottom={bottom}
          left={left}
          right={right}
          big={big}
          cascade={cascade}
        >
          <div className={`card-container`}>
            <div className={`card-body shadow ${display} ${styleClass}`}>
              <div className="card-image">
                <img
                  src={image.url}
                  alt={image.alt}
                  width={image.width}
                  height={image.height}
                />
              </div>
              <div className="card-text">
                <div
                  dangerouslySetInnerHTML={{
                    __html: `${sanitize(text)}`,
                  }}
                />
              </div>
            </div>
          </div>
        </Fade>
      );
    }
  } else {
    if (pageLink) {
      return (
        <div className={`card-container`}>
          <HashLink
            smooth
            to={
              pageLink.content_type === "flex.homepage"
                ? "/"
                : `/${pageLink.slug}#${anchor}`
            }
          >
            <div
              className={`card-link card-body shadow ${display} ${styleClass}`}
            >
              <div className="card-image">
                <img
                  src={image.url}
                  alt={image.alt}
                  width={image.width}
                  height={image.height}
                />
              </div>
              <div className="card-text">
                <div
                  dangerouslySetInnerHTML={{
                    __html: `${sanitize(props.value.text)}`,
                  }}
                />
              </div>
            </div>
          </HashLink>
        </div>
      );
    } else {
      return (
        <div className={`card-container`}>
          <div className={`card-body shadow ${display} ${styleClass}`}>
            <div className="card-image">
              <img
                src={image.url}
                alt={image.alt}
                width={image.width}
                height={image.height}
              />
            </div>
            <div className="card-text">
              <div
                dangerouslySetInnerHTML={{
                  __html: `${sanitize(text)}`,
                }}
              />
            </div>
          </div>
        </div>
      );
    }
  }
}

export { Card };
