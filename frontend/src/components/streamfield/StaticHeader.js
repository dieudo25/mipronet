import React from "react";

import { sanitize } from "dompurify";

import { Button } from "../elements/Button";

import styled from "styled-components";

function StaticHeader(props) {
  const image = props.value.image;
  const image_phone = props.value.image_phone;
  const title = props.value.title;
  const description = props.value.description;
  const classes = props.value.classes;
  const button = props.value.description.button;

  const StyledImage = styled.div`
    background-image: url(${image_phone.url});

    @media (min-width: 768px) {
      background-image: url(${image.url});
    }
  `;

  if (button) {
    const slug = button.slug;
    const contentType = button.content_type;
    const button_text = button.button_text;

    return (
      <StyledImage className={`jumbotron bg-cover ${classes}`}>
        <div className="header-container">
          <h1>{title}</h1>
          <div
            className="fullscreen-paraph"
            dangerouslySetInnerHTML={{
              __html: `${sanitize(description.text)}`,
            }}
          />
          <div className="banner-btn">
            <Button
              contentType={contentType}
              slug={slug}
              buttonText={button_text}
            />
          </div>
        </div>
      </StyledImage>
    );
  } else {
    return (
      <StyledImage className={`jumbotron bg-cover ${classes}`}>
        <div className="header-container">
          <h1>{title}</h1>
          <div
            className="fullscreen-paraph"
            dangerouslySetInnerHTML={{
              __html: `${sanitize(description.text)}`,
            }}
          />
        </div>
      </StyledImage>
    );
  }
}

export { StaticHeader };
