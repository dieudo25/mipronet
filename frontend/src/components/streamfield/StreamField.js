import React from "react";
import { sanitize } from "dompurify";
import { SlickSlider } from "./slider/slickSlider/SlickSlider";
import { CustomParallax } from "./Parallax";
import { FullScreenSlider } from "./FullScreenSliderItem";
import { Form } from "./form/Form";
import { EmbedMaps } from "./maps/GoogleMaps";
import { TwoColumn, ThreeColumn, FourColumn } from "./Column";
import { Heading } from "../elements/headings";
import { OffSetBlock } from "../elements/OffSetBlock";
import { Card } from "./Card";
import { Image, AnimatedImage } from "./Image";
import { Cta } from "./Cta.js";
import { Button } from "../elements/Button";
import { StaticHeader } from "./StaticHeader";

function StreamField(props) {
  // We build a StreamField Component, which iterate the props.value
  const streamField = props.value;
  let html = [];

  for (let i = 0; i < streamField.length; i++) {
    const field = streamField[i];

    // Decide which block component should be used according to the block type
    switch (field.type) {
      case "offset":
        html.push(
          <OffSetBlock value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "maps":
        html.push(<EmbedMaps value={field.value} key={`${i}.${field.type}`} />);
        break;
      case "streamform":
        html.push(<Form value={field.value} key={`${i}.${field.type}`} />);
        break;
      case "slider":
        html.push(
          // pass field.value to the child Component props,
          // so they would use the field.value to render HTML.
          // The key is used to distinguish child in a list React keys
          <SlickSlider value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "parallax":
        html.push(
          <CustomParallax value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "image":
        html.push(
          <div className="img-container">
            <Image value={field.value} key={`${i}.${field.type}`} />
          </div>
        );
        break;
      case "animated_image":
        html.push(
          <AnimatedImage value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "headings":
        html.push(<Heading value={field.value} key={`${i}.${field.type}`} />);
        break;
      case "button":
        html.push(
          <Button
            value={field.value}
            streamfield={true}
            key={`${i}.${field.type}`}
          />
        );
        break;
      case "cards":
        switch (props.col) {
          case 2:
            html.push(
              <div className="col-item" key={`${field.type}.${i}`}>
                <Card value={field.value} />
              </div>
            );
            break;
          case 3:
            html.push(
              <div className="col-item" key={`${field.type}.${i}`}>
                <Card value={field.value} />
              </div>
            );
            break;
          case 4:
            html.push(
              <div className="col-item" key={`${field.type}.${i}`}>
                <Card value={field.value} />
              </div>
            );
            break;
          default:
            html.push(<Card value={field.value} key={`${i}.${field.type}`} />);
        }
        break;
      case "two_column":
        html.push(<TwoColumn value={field.value} key={`${i}.${field.type}`} />);
        break;
      case "three_column":
        html.push(
          <ThreeColumn value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "four_column":
        html.push(
          <FourColumn value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "text":
        html.push(
          <div key={`${i}.${field.type}`} className="text-stream">
            <div
              dangerouslySetInnerHTML={{ __html: `${sanitize(field.value)}` }}
            />
          </div>
        );
        break;
      case "rich_text_button":
        html.push(
          <Cta
            key={`${i}.${field.type}`}
            className="cta-block"
            value={field.value}
          />
        );
        break;
      case "full_screen":
        html.push(
          <FullScreenSlider value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      case "static_header":
        html.push(
          <StaticHeader value={field.value} key={`${i}.${field.type}`} />
        );
        break;
      default:
        html.push(
          <div value={field.value} key={`${i}.${field.type}`}>
            <h2 class="text-danger">
              {field.value} | {field.type} not implemented in StreamField.js
            </h2>
          </div>
        );
    }
  }

  return html;
}

export { StreamField };
