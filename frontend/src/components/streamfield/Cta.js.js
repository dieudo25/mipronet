import React from "react";
import { sanitize } from "dompurify";
import Fade from "react-reveal/Fade";

import "../../style/index.scss";

import { Button } from "../elements/Button";

function Cta(props) {
  const text = props.value.text;
  const button_text = props.value.button_text;
  const button = props.value.button;
  const containerWidth = props.value.container_width;
  const styleClasses = props.value.style_classes;
  const id = props.value.block_id;
  const animation = props.value.animation.reveal;
  const animationBig = props.value.animation.reveal_big;
  const animationCascade = props.value.animation.reveal_cascade;

  let top = false;
  let bottom = false;
  let left = false;
  let right = false;
  let big = false;
  let cascade = false;

  if (animation) {
    switch (animation) {
      case "top":
        top = true;
        break;
      case "bottom":
        bottom = true;
        break;
      case "left":
        left = true;
        break;
      case "right":
        right = true;
        break;
      default:
        break;
    }
    if (animationBig) {
      big = true;
    }
    if (animationCascade) {
      cascade = true;
    }

    if (button) {
      return (
        <div
          id={id ? id : ""}
          className={
            containerWidth === "normal-width"
              ? "normal-container"
              : "full-width-container"
          }
        >
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={`parallax-cta ${styleClasses}`}>
              <div
                dangerouslySetInnerHTML={{
                  __html: `${sanitize(text)}`,
                }}
              />
              <div className="parallax-btn">
                <Button
                  contentType={button.contentType}
                  slug={button.slug}
                  buttonText={button_text}
                />
              </div>
            </div>
          </Fade>
        </div>
      );
    } else {
      return (
        <div
          id={id ? id : ""}
          className={
            containerWidth === "normal-width"
              ? "normal-container"
              : "full-width-container"
          }
        >
          <Fade
            top={top}
            bottom={bottom}
            left={left}
            right={right}
            big={big}
            cascade={cascade}
          >
            <div className={`parallax-cta ${styleClasses}`}>
              <div
                dangerouslySetInnerHTML={{
                  __html: `${sanitize(text)}`,
                }}
              />
            </div>
          </Fade>
        </div>
      );
    }
  } else {
    if (button) {
      return (
        <div
          id={id ? id : ""}
          className={
            containerWidth === "normal-width"
              ? "normal-container"
              : "full-width-container"
          }
        >
          <div className={`parallax-cta ${styleClasses}`}>
            <div
              dangerouslySetInnerHTML={{
                __html: `${sanitize(text)}`,
              }}
            />
            <div className="parallax-btn">
              <Button
                contentType={button.contentType}
                slug={button.slug}
                buttonText={button_text}
              />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div
          id={id ? id : ""}
          className={
            containerWidth === "normal-width"
              ? "normal-container"
              : "full-width-container"
          }
        >
          <div className={`parallax-cta ${styleClasses}`}>
            <div
              dangerouslySetInnerHTML={{
                __html: `${sanitize(text)}`,
              }}
            />
          </div>
        </div>
      );
    }
  }
}

export { Cta };
