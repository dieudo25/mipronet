import React from "react";
import Fade from "react-reveal/Fade";

import "../../style/index.scss";

function Image(props) {
  let url = props.value.url;
  let width = props.value.width;
  let height = props.value.height;
  let alt = props.value.alt;

  return <img src={url} width={width} height={height} alt={alt} />;
}

function AnimatedImage(props) {
  let url = props.value.image.url;
  let width = props.value.image.width;
  let height = props.value.image.height;
  let alt = props.value.image.alt;
  let classes = props.value.classes;

  let animation = props.value.animation.reveal;
  let animationBig = props.value.animation.reveal_big;
  let animationCascade = props.value.animation.reveal_cascade;

  let top = false;
  let bottom = false;
  let left = false;
  let right = false;
  let big = false;
  let cascade = false;

  if (animation) {
    switch (animation) {
      case "top":
        top = true;
        break;
      case "bottom":
        bottom = true;
        break;
      case "left":
        left = true;
        break;
      case "right":
        right = true;
        break;
      default:
        break;
    }
    if (animationBig) {
      big = true;
    }
    if (animationCascade) {
      cascade = true;
    }

    return (
      <Fade
        top={top}
        bottom={bottom}
        left={left}
        right={right}
        big={big}
        cascade={cascade}
      >
        <div className={`img-container ${classes}`}>
          <img src={url} width={width} height={height} alt={alt} />
        </div>
      </Fade>
    );
  } else {
    return (
      <div className="img-container">
        <img src={url} width={width} height={height} alt={alt} />;
      </div>
    );
  }
}

export { Image, AnimatedImage };
