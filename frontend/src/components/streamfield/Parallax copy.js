import React from "react";
import { Parallax } from "react-parallax";
import cardImage from "../../../stories/assets/image_1.jpg";

function NormalParallax(props) {
  return (
    <>
      <div style={{ height: "500px" }}></div>

      <Parallax bgImage={cardImage} strengh={500}>
        <div style={{ height: 500 }}>
          <div style={insideStyles}> HTML inside the parallax</div>
        </div>
      </Parallax>
      <div style={{ height: "500px" }}></div>
      <Parallax bgImage={cardImage} blur={{ min: -3, max: 5 }}>
        <div style={{ height: 500 }}>
          <div style={insideStyles}>Dynamic Blur</div>
        </div>
      </Parallax>
      <div style={{ height: "500px" }}></div>
      <Parallax bgImage={cardImage} strength={-100}>
        <div style={{ height: 500 }}>
          <div style={insideStyles}>Reverse direction</div>
        </div>
      </Parallax>
      <div style={{ height: "500px" }}></div>
      <Parallax
        bgImage={cardImage}
        strength={200}
        renderLayer={(percentage) => (
          <div>
            {console.log(percentage)}
            <div
              style={{
                position: "absolute",
                background: `rgba(255, 125, 0, ${percentage * 1})`,
                left: "50%",
                top: `${percentage * 50}%`,
                borderRadius: `${percentage * 50}%`,
                transform: "translate(-50%,-50%)",
                width: percentage * 400,
                height: percentage * 400,
              }}
            >
              <div
                style={{
                  opacity: percentage * 1,
                  background: "white",
                  padding: 20,
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%,-50%)",
                }}
              >
                renderProp
              </div>
            </div>
          </div>
        )}
      >
        <div style={{ height: 500 }}></div>
      </Parallax>
      <div style={{ height: "500px" }}></div>
    </>
  );
}

const insideStyles = {
  background: "white",
  padding: 20,
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
};

export { NormalParallax };
