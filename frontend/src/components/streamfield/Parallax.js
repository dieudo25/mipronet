import React from "react";
import { Parallax } from "react-parallax";
import { StreamField } from "./StreamField";

function CustomParallax(props) {
  const image = props.value.background_image.image;
  const parallax_strengh = props.value.background_image.parallax_strengh;
  const parallax_blur_min = props.value.background_image.parallax_blur_min;
  const parallax_blur_max = props.value.background_image.parallax_blur_max;
  const style_classes = props.value.style_classes;
  const content = props.value.content;

  return (
    <>
      <Parallax
        bgImage={image.url}
        strengh={parallax_strengh}
        blur={{ min: parallax_blur_min, max: parallax_blur_max }}
        className={style_classes}
      >
        <StreamField value={content} />
      </Parallax>
    </>
  );
}

export { CustomParallax };
