import React from "react";

function EmbedMaps(props) {
  const api_key = "AIzaSyAsJUK4CzrjhdJ8gwbpF5bfcng_mp6Ct8s";

  return (
    <iframe
      title="Mipronet Services Geo location"
      style={{ border: 0, width: "100%", height: "500px" }}
      loading="lazy"
      allowFullScreen
      src={`https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ2QioZ9kxjEcRtXRYhpMsFyM&key=${api_key}`}
    ></iframe>
  );
}

export { EmbedMaps };
