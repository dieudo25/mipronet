from rest_framework.fields import Field
from .models import SocialMenu
from wagtail.images.api.fields import ImageRenditionField


class SocialItemField(Field):
    """
        Custom SocialItemField for Rest API
    """

    def to_representation(self, socialItems):
        """
            Overwrite to_representation method
            Define how the data is represented in JSON

        """

        try:
            return[
                {
                    "id": item.id,
                    "link_url": item.link_url,
                    "sort_order": item.sort_order,
                    "logo_image": ImageRenditionField("width-64").to_representation(item.logo_image),

                } for item in socialItems.all()
            ]
        except Exception:
            return ["error"]
