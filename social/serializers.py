from rest_framework import serializers

from .models import SocialMenu, SocialMenuItem
from .fields import SocialItemField

from wagtail.images.models import Image
from wagtail.images.api.fields import ImageRenditionField


class SocialMenuSerializer(serializers.ModelSerializer):
    api_get_social_items = SocialItemField(source="get_social_items")

    class Meta:
        model = SocialMenu
        fields = [
            "id",
            "title",
            "slug",
            "api_get_social_items",
        ]

    def get_items(self, obj):
        return obj.api_get_social_items


class SocialItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMenuItem
        fields = "__all__"

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.logo_image:
            representation['logo_image'] = ImageRenditionField(
                "width-64").to_representation(instance.logo_image)
        return representation
