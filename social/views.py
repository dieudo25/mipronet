from rest_framework import viewsets

from .models import SocialMenuItem, SocialMenu
from .serializers import SocialMenuSerializer, SocialItemSerializer


class SideBarSocialMenuSet(viewsets.ModelViewSet):
    serializer_class = SocialMenuSerializer
    queryset = SocialMenu.objects.filter(slug="sidebar")
    http_method_names = ["get"]


class SidebarSocialItemSet(viewsets.ModelViewSet):
    serializer_class = SocialItemSerializer
    queryset = SocialMenuItem.objects.filter(
        social_menu__slug="sidebar")
    http_method_names = ["get"]


class FooterSocialMenuSet(viewsets.ModelViewSet):
    serializer_class = SocialMenuSerializer
    queryset = SocialMenu.objects.filter(slug="footer")
    http_method_names = ["get"]


class FooterSocialItemSet(viewsets.ModelViewSet):
    serializer_class = SocialItemSerializer
    queryset = SocialMenuItem.objects.filter(
        social_menu__slug="footer")
    http_method_names = ["get"]
