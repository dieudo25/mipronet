from django.db import models

from django_extensions.db.fields import AutoSlugField

from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.core.models import Orderable
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet


class SocialMenuItem(Orderable):
    """
        Social Media Settings
    """

    logo_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    link_url = models.URLField(
        blank=True, null=True, help_text="URL vers le réseaux social")
    social_menu = ParentalKey(
        "SocialMenu", related_name="social_menu_items")

    panels = [
        ImageChooserPanel("logo_image"),
        FieldPanel("link_url"),
    ]


@register_snippet
class SocialMenu(ClusterableModel):
    """ Social Media Settings for the Web APP"""

    title = models.CharField(max_length=100, blank=True, null=True)
    slug = AutoSlugField(populate_from="title", editable=True)

    panels = [
        FieldPanel("title"),
        FieldPanel("slug"),
        InlinePanel("social_menu_items", label="Liste de réseaux sociaux")
    ]

    def __str__(self):
        return self.title

    # Add custom property to API
    @property
    def get_social_items(self):
        return SocialMenuItem.objects.filter(social_menu=self.pk)
