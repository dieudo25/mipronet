from rest_framework import routers

from .views import (
    SideBarSocialMenuSet,
    SidebarSocialItemSet,
    FooterSocialMenuSet,
    FooterSocialItemSet
)


# Rest Framework API
social_router = routers.DefaultRouter()

social_router.register(r"sidebar/items", SidebarSocialItemSet)
social_router.register(r"sidebar", SideBarSocialMenuSet)
social_router.register(r"footer/items", FooterSocialItemSet)
social_router.register(r"footer", FooterSocialMenuSet)
