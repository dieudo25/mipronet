from rest_framework import serializers

from wagtailstreamforms.models import FormSubmission


class FormSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormSubmission
        fields = "__all__"
