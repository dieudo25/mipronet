from rest_framework import routers

from .views import FormSubmissionSet


# Rest Framework API
form_router = routers.DefaultRouter()

form_router.register(r"submissons", FormSubmissionSet)
