import json

from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage

from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from wagtailstreamforms.models import FormSubmission

from .serializers import FormSubmissionSerializer

from django.conf import settings

from django.core.mail import send_mail
from django.template.loader import render_to_string


class FormSubmissionSet(viewsets.ModelViewSet):
    serializer_class = FormSubmissionSerializer
    queryset = FormSubmission.objects.all()
    http_method_names = ["post"]
    permission_classes = [AllowAny]

    def create(self, request):

        serializer = FormSubmissionSerializer(data=request.data)
        if serializer.is_valid():  # MAGIC HAPPENS HERE
            serializer.save()

            data = request.data["form_data"]
            json_data = json.loads(data)
            context = {
                'data': json_data
            }

            msg_plain = render_to_string('form/contact.txt', context)
            msg_html = render_to_string('form/contact.html', context)

            send_mail(
                'Mipronet Services - Nouvelle demande de devis !',
                msg_plain,
                settings.EMAIL_HOST_USER,
                ['twahirwa25@gmail.com', settings.EMAIL_HOST_USER],
                html_message=msg_html,
            )

            return super().create(request)
        return super().create(request)
