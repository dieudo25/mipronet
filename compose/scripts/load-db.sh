#!/bin/bash
set -e


DUMP_LOCATION=/tmp/db/data.pgsql

psql -U $POSTGRES_USER $POSTGRES_DB < $DUMP_LOCATION
