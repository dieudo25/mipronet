#!/bin/bash
set -e


DUMP_DESTINATION=/tmp/data/data.pgsql

pg_dump -U $POSTGRES_USER $POSTGRES_DB > $DUMP_DESTINATION
