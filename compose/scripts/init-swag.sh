#!/bin/bash
set -e

REMOTE_SERVER_USER=root
SSH_KEY=mipronet
REMOTE_TOKEN_LOCATION=/app/compose/production/swag/config/dns-conf
REMOTE_NGINX_LOCATION=/app/compose/production/swag/config/nginx/site-confs
LOCAL_TOKEN_LOCATION=compose/production/nginx
DNS_NAME=digitalocean.ini
NGINX_CONF_NAME=default
REMOTE_SERVER=$IP_MIPRONET_APP


printf "######################################################################################\n\n"
printf "Change SWAG dns configuration\n\n"
printf "######################################################################################\n\n"
ssh $REMOTE_SERVER_USER@$IP_MIPRONET_APP << ENDSSH
    rm $REMOTE_TOKEN_LOCATION/$DNS_NAME
    rm $REMOTE_NGINX_LOCATION/$NGINX_CONF_NAME
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "DELETE REMOTE CONFIG FILES: SUCCESS\n\n"
else
  printf "DELETE REMOTE CONFIG FILES: FAILURE\n\n"
  exit 1
fi


printf "######################################################################################\n\n"
printf "Send configuration file to remote server\n\n"
printf "######################################################################################\n\n"

rsync $LOCAL_TOKEN_LOCATION/$DNS_NAME $REMOTE_SERVER_USER@$REMOTE_SERVER:$REMOTE_TOKEN_LOCATION
rsync $LOCAL_TOKEN_LOCATION/$NGINX_CONF_NAME $REMOTE_SERVER_USER@$REMOTE_SERVER:$REMOTE_NGINX_LOCATION

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "SEND CONFIG FILES TO REMOTE SERVER: SUCCESS\n\n"
else
  printf "SEND CONFIG FILES TO REMOTE SERVER: FAILURE\n\n"
  exit 1
fi


printf "######################################################################################\n\n"
printf "Restart service\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
    docker-compose -f /app/docker-compose.prod.yml restart
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "SERVICE RESTART: SUCCESS\n\n"
else
  printf "SERVICE RESTART: FAILURE\n\n"
  exit 1
fi