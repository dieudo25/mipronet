#!/bin/bash

SSH_KEY=mipronet

FILE=mediafiles.tgz

LOCAL_PATH=media/*

REMOTE_CONTAINER=app_web_1
VOLUME_PATH=/app/compose/production/django/data

UNTAR_PATH=/app
MEDIA_DIRECTORY=media

# Create mediafiles zip
printf "######################################################################################\n\n"
printf "Compress media files\n\n"
printf "######################################################################################\n\n"
tar -czvf $FILE $LOCAL_PATH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "COMPRESS SUCCESS: SUCCESS\n\n"
else
  printf "COMPRESS SUCCESS: FAILURE\n\n"
  exit 1
fi

# Send mediafiles zip to server
printf "######################################################################################\n\n"
printf "Send media files to $IP_MIPRONET_APP\n\n"
printf "######################################################################################\n\n"
rsync  ./$FILE root@$IP_MIPRONET_APP:$VOLUME_PATH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "SEND MEDIA FILES: SUCCESS\n\n"
else
  printf "SEND MEDIA FILES: FAILURE\n\n"
  exit 1
fi


printf "######################################################################################\n\n"
printf "Restart Django Container\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
  docker-compose -f /app/docker-compose.prod.yml restart
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "RESTART WEB CONTAINER: SUCCESS\n\n"
else
  printf "RESTART WEB CONTAINER: FAILURE\n\n"
  exit 1
fi

# Untar the mediafiles to the REMOTE_CONTAINER /app folder
printf "######################################################################################\n\n"
printf "Untar media files to $IP_MIPRONET_APP\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
    printf "\n\nDelete media folder\n"
    docker exec $REMOTE_CONTAINER rm -rf $UNTAR_PATH/$MEDIA_DIRECTORY
    printf "\n\nUncompress media files\n"
    docker exec $REMOTE_CONTAINER tar -xvf $VOLUME_PATH/$FILE --no-same-permissions --directory=$UNTAR_PATH
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "UNTAR MEDIA FILES: SUCCESS\n\n"
else
  printf "UNTAR MEDIA FILES: FAILURE\n\n"
  exit 1
fi

rm $FILE

exit 0