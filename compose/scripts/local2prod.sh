#!/bin/bash 

# Please run script in the $project_name/scripts folder

# Before running the script make sure these environnement variables are set inthe .env/.prod-sample :
# POSTGRES_DB
# POSTGRES_USER

# Fill with the name of the docker-container
SSH_KEY=mipronet

LOCAL_CONTAINER=mipronet_db_1
REMOTE_CONTAINER=app_db_1

REMOTE_VOLUME_FOLDER=/app/compose/production/db
LOCAL_VOLUME_FOLDER=/home/dieudo/Dev/pro/mipronet.ch/mipronet/compose/local/db

REMOTE_SERVER=$IP_MIPRONET_APP
REMOTE_USER=root

CONTAINER_SCRIPTS_PATH=/scripts
LOCAL_SCRIPTS_PATH=/home/dieudo/Dev/pro/mipronet.ch/mipronet/compose/scripts


########################################################################################################


# Check if the variable are empty 
printf "\n\n######################################################################################\n\n"
printf "Check variable for running the script\n\n"
printf "######################################################################################\n\n"

if [ -z "$SSH_KEY" ]; then
    printf "Please fill SSH_KEY before running the script\n\n"
    exit 1
elif [ -z "$LOCAL_CONTAINER" ]; then
    printf "Please fill LOCAL_CONTAINER before running the script\n\n"
    exit 1
elif [ -z "$REMOTE_CONTAINER" ]; then
    printf "Please fill REMOTE_CONTAINER before running the script\n\n"
    exit 1
elif [ -z "$REMOTE_VOLUME_FOLDER" ]; then
    printf "Please fill REMOTE_VOLUME_FOLDER before running the script\n\n"
    exit 1
elif [ -z "$LOCAL_VOLUME_FOLDER" ]; then
    printf "Please fill LOCAL_VOLUME_FOLDER before running the script\n\n"
    exit 1
elif [ -z "$REMOTE_SERVER" ]; then
    printf "Please fill REMOTE_SERVER before running the script\n\n"
    exit 1
elif [ -z "$REMOTE_USER" ]; then
    printf "Please fill REMOTE_USER before running the script\n\n"
    exit 1
elif [ -z "$CONTAINER_SCRIPTS_PATH" ]; then
    printf "Please fill CONTAINER_SCRIPTS_PATH before running the script\n\n"
    exit 1
elif [ -z "$LOCAL_SCRIPTS_PATH" ]; then
    printf "Please fill LOCAL_SCRIPTS_PATH before running the script\n\n"
    exit 1
fi


########################################################################################################


printf "######################################################################################\n\n"
printf "Restart Postgres Container\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
  docker-compose -f /app/docker-compose.prod.yml restart db
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "RESTART DB CONTAINER: SUCCESS\n\n"
else
  printf "RESTART DB CONTAINER: FAILURE\n\n"
  exit 1
fi

# Create USER AND DB
printf "######################################################################################\n\n"
printf "Create User  and DB\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
  docker exec $REMOTE_CONTAINER bash $CONTAINER_SCRIPTS_PATH/init-user-db.sh
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "CREATION USER AND DB: SUCCESS\n\n"
else
  printf "CREATION USER AND DB: FAILURE\n\n"
  exit 1
fi


########################################################################################################


# Start Mocal container
printf "######################################################################################\n\n"
printf "Start local container\n\n"
printf "######################################################################################\n\n"

docker-compose up -d

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "START LOCAL CONTAINER : SUCCESS\n\n"
else
  printf "START LOCAL CONTAINER : FAILURE\n\n"
  exit 1
fi


########################################################################################################


# DUMP DATA IN THE LOCAL CONTAINER OF THE DB
printf "######################################################################################\n\n"
printf "Dump data of $LOCAL_CONTAINER\n\n"
printf "######################################################################################\n\n"

docker exec $LOCAL_CONTAINER bash $CONTAINER_SCRIPTS_PATH/dump-db.sh

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "DUMP DATA: SUCCESS\n\n"
else
  printf "DUMP DATA: FAILURE\n\n"
  exit 1
fi


########################################################################################################


# SEND DATA TO REMOTE SERVER
printf "######################################################################################\n\n"
printf "Send db data to remote server\n\n"
printf "######################################################################################\n\n"

rsync $LOCAL_VOLUME_FOLDER/data.pgsql $REMOTE_USER@$IP_MIPRONET_APP:$REMOTE_VOLUME_FOLDER/data.pgsql

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "SEND DATA: SUCCESS\n\n"
else
  printf "SEND DATA: FAILURE\n\n"
  exit 1
fi

########################################################################################################


# LOAD DUMP IN DB
printf "######################################################################################\n\n"
printf "Load dump data in new db\n\n"
printf "######################################################################################\n\n"

sleep 5

ssh $SSH_KEY << ENDSSH
  docker exec $REMOTE_CONTAINER bash $CONTAINER_SCRIPTS_PATH/load-db.sh
ENDSSH

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "LOAD DATA: SUCCESS\n\n"
else
  printf "LOAD DATA: FAILURE\n\n"
  exit 1
fi


########################################################################################################


# MIGRATE MEDIA FILES
printf "######################################################################################\n\n"
printf "Migration of the mediafiles\n\n"
printf "######################################################################################\n\n"

bash $LOCAL_SCRIPTS_PATH/migrate-media.sh

# Get status
status=$?

if [ $status -eq 0 ]
then
  printf "MIGRATE MEDIA FILES: SUCCESS\n\n"
else
  printf "MIGRATE MEDIA FILES: FAILURE\n\n"
  exit 1
fi