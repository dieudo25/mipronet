from django.contrib.contenttypes.models import ContentType

from wagtail.api.v2.views import PagesAPIViewSet
from wagtail_headless_preview.models import PagePreview
from rest_framework.response import Response
from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.api.v2.views import PagesAPIViewSet
from wagtail.documents.api.v2.views import DocumentsAPIViewSet
from wagtail.images.api.v2.views import ImagesAPIViewSet


class PagePreviewAPIViewSet(PagesAPIViewSet):
    # Query string parameters
    known_query_parameters = PagesAPIViewSet.known_query_parameters.union(
        ['content_type', 'token'])

    def listing_view(self, request):
        page = self.get_object()
        serializer = self.get_serializer(page)
        return Response(serializer.data)

    def detail_view(self, request, pk):
        page = self.get_object()
        serializer = self.get_serializer(page)
        return Response(serializer.data)

    def get_object(self):
        app_label, model = self.request.GET['content_type'].split('.')

        # Get content type
        content_type = ContentType.objects.get(
            app_label=app_label, model=model)

        # Get preview page wich contains JSON representation of the draft content
        page_preview = PagePreview.objects.get(
            content_type=content_type, token=self.request.GET["token"]
        )
        page = page_preview.as_page()
        if not page.pk:
            # fake primary key to stop API URL routing from complaining
            page.pk = 0

        return page


# Wagtail API
cms_api_router = WagtailAPIRouter("wagtailapi")

cms_api_router.register_endpoint("pages", PagesAPIViewSet)
cms_api_router.register_endpoint("images", ImagesAPIViewSet)
cms_api_router.register_endpoint("documents", DocumentsAPIViewSet)
cms_api_router.register_endpoint("page_preview", PagePreviewAPIViewSet)
